import math


def inputing():
    x = float(input("Enter the angle in radian measure, whose sine is to be calculated : "))
    return x

def maclaurin_of_sine(x):
    sum1 = 0
    y = x**2
    for i in range(1, 7, 1):
        sum1 = sum1 + x
        x = - ((x * y) / ((2 * i + 1) * (2 * i)) )
    return sum1

def output(n, x):
    print("The sine of {} is {}".format(n, x))


def main():
    x = inputing()
    s = maclaurin_of_sine(x)
    output(x, s)


main()