def Input():
        s = input("Enter the string : ")
        return s

def CStoDictionary(s):
        dictionary = { x : y for x, y in (a.split('=') for a in s.split(';'))}
        return dictionary

def output(string):
        print(string)

def main():
        string = Input()
        dictionary = CStoDictionary(string)
        output(dictionary)

main()
        
        
                
