def input1():
    a=int(input("Enter the first number: "))
    b=int(input("Enter the second number: "))
    return a,b
def compute(a,b):
    c=a+b
    return c
def display1(a,b,c):
    print("The sum of",a,"and",b,"is",c)
def display2(a,b,c):
    print("%d + %d = %d"%(a,b,c))
def display3(a,b,c):
    print("The sum of {} and {} is {} \n".format(a, b, c))
def vif():
    a,b=input1()
    c=compute(a,b)
    display1(a,b,c)
    display2(a,b,c)
    display3(a,b,c)
vif() #vif stands for very important function
