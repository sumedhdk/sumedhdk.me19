import math
def input1():
	print "Enter the coordinates of the first point\n"
	x,y = float(input()),float(input())
	return x,y
def input2():
	print "Enter the coordinates of the second point\n"
	x,y = float(input()),float(input())
	return x,y
def compute(x1,y1,x2,y2):
	d=math.sqrt(((x1-x2)**2)+((y2-y1)**2))
	return d
def display(x1,y1,x2,y2,dis):
	print "The distance between the points (%f,%f) and (%f,%f) is %f\n"%(x1,y1,x2,y2,dis)
x1,y1=input1()
x2,y2=input2()
dis=compute(x1,y1,x2,y2)
display(x1,y1,x2,y2,dis)