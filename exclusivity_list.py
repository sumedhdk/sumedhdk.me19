def get_list(l):
        lst = [ ]
        e = input()
        for i in range (0,l):
                lst.append(e)
        return lst

def search(lst, key):
        if key in lst:
                return True
        return False

def get_disjoint(l1, l2):
        lst1 = [ ]
        lst2 = [ ]
        l1 = [x.lower() for x in l1]
        l2 = [x.lower() for x in l2]
        for i in l1:
                if(search(l2, i)):
                        lst2.append(i)
                else :
                        lst1.append(i)

        return lst1, lst2

def main():
        n1 = 47
        n2 = 55
        l1 = get_list(n1)
        l2 = get_list(n2)
        l3, l4 = get_disjoint(l1, l2)
        l5, l6 = get_disjoint(l2, l1)
        print('l4 = {}'.format(l4))
        if(l6 == l4):
                print("You are right")
