import sympy
from sympy import Poly
from sympy import var

def Input():
    n = int(input("Enter the number for the primality test:"))
    return n

def get_co(p):
    x = sympy.symbols('x')
    exp = ((x - 1)**p) - (x**p - 1)
    a = Poly(exp, x)
    lst1 = []
    lst1 = a.coeffs()
    return lst1

def check_coeffs(p, lst):
    flag = 0
    for i in lst :
        if(i % p == 0):
            flag = 1
            break
    return flag

def output(f, p):
    if(f==1):
        print("The {} is a prime number".format(p))
    else:
        print("The {} is NOT a prime number".format(p))

def main():
    p = Input()
    lst = []
    lst = get_co(p)
    f = check_coeffs(p, lst)
    output(f, p)

main()





    
