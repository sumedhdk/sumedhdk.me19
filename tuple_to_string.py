def Input():
        s = input("Enter the string : ")
        return s

def CStoListOfTuples(s):
        list_of_tuples = [ ]
        list_of_tuples =[tuple(a.split('=')) for a in s.split(';')]
        return list_of_tuples

def CListOfTuplestoString(lst):
        s = ''
        s= ';'.join(["=".join(i) for i in lst])
        return s

def output(string):
        print(string)

def main():
        string = Input()
        list_of_tuples = CStoListOfTuples(string)
        final_string = CListOfTuplestoString(list_of_tuples)
        output(final_string)

main()
        
        