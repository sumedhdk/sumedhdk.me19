import math

def Input():
    n = int(input("Enter the number of numbers that you will enter : "))
    return n


def Input_list(n):
    num_list = []
    print("Enter the elements")
    for i in range(0, n, 1):
        e = int(input())
        num_list.append(e)
    return num_list


def isprime(a):
    if(a <= 1):
            return False
    elif(a <= 3):
        return True
    elif(a % 2 == 0 or a % 3 == 0):
        return False
    else :
        p = int(math.sqrt(a))
        for i in range(5, p + 1, 6):
            if(((a % i) == 0) or ((a % (i+2)) == 0) ):
                    return False
    return True


def sumofprime(lst):
        sum1 = 0
        sum1 = sum(x for x in lst if isprime(x))
        return sum1


def output(sum1):
    print("The the sum of all prime numbers is {}\n".format(sum1)) 


def main():
    n = Input()
    lst = []
    lst = Input_list(n)
    sum1 = sumofprime(lst)
    output(sum1)

main()