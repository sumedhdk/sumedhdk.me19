import cmath


def inputing():
    a = float(input("Enter the coeffiecient of the second degree term : "))
    b = float(input("Enter the coeffiecient of the first degree term : "))
    c = float(input("Enter the constant term : "))
    return a, b, c


def calculate(a, b, c):
    d = (b**2) - (4*a*c)
    r1 = complex(((-b)/(2*a)) +  cmath.sqrt(d))
    r2 = complex(((-b)/(2*a)) - cmath.sqrt(d))
    return r1, r2


def output(a, b):
     print("The roots of the quadratic equation are : {} and {}\n".format(a, b))


def main():
    a, b, c = inputing()
    r1, r2 = calculate(a, b, c)
    output(r1, r2)


main()

         
         
