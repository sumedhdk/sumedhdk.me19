def input_number_of_cases():
        n = int(input())
        return n


def input_all_numbers(n):
        listOfNumbers = []
        for i in range(0, n, 1):
                e = int(input())
                listOfNumbers.append(e)
        return listOfNumbers


def computation_for_a_number(a):
        lst = [ ]
        lst.append(3)
        for i in range(1, a, 1):
                if( i % 2 == 0 ):
                        value = i * 2
                        lst.append(value)
                else:
                        value = i ** 2
                        lst.append(value)
        return lst


def compute_all_numbers(n, lon):
        list_for_a_number = [ ]
        list_for_all_numbers = [ list_for_a_number] * n
        for i in range(0, n, 1):
                list_for_a_number = computation_for_a_number(lon[i])
                list_for_all_numbers[i] =  list_for_a_number
        return list_for_all_numbers


def output(n, loan, lon):
        for i in range(0, n, 1):
                for j in range(0, lon[i] - 1, 1):
                        print(loan[i][j], end=' ')
                print('{}'.format(loan[ i ][ lon[i]-1 ]))


if __name__ == '__main__':
        n = input_number_of_cases()
        lon = []
        lon = input_all_numbers(n)
        lfan = []
        loan = [lfan] * n
        loan = compute_all_numbers(n, lon)
        output(n, loan, lon)
        
